package com.example.final_project

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.widget.Toolbar
import java.util.*

const val KEY_LATITUDE = "lat"
const val KEY_LONGITUDE = "lon"
const val KEY_TIMETAG = "timetag"
const val KEY_NAME = "name"

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Toolbar initialize
        var tl: Toolbar = findViewById(R.id.TOOLBAR_MAIN)
        tl.setNavigationIcon(R.drawable.ncu_lite)
        tl.setTitleTextColor(resources.getColor(R.color.White))
        tl.setTitle("腳踏車監測系統")

        // Orientation will not change when the cell phone tilt.
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        // Cell phone will keep screen on.
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        window.setTitle("腳踏車位置監視")

        var bt_map:Button = findViewById(R.id.BT_MAP)
        bt_map.setOnClickListener(this)
        var bt_data:Button = findViewById(R.id.BT_DATA)
        bt_data.setOnClickListener(this)

        // Get the bike user
        myWeb.updateBikeUsers()
        // Create Adapter
        var arrAda = ArrayAdapter(this, R.layout.my_spinner_style, myWeb.bikeUser)
        var sp:Spinner = findViewById(R.id.SPINNER)
        arrAda.setDropDownViewResource(R.layout.my_spinner_style)
        sp.adapter = arrAda
        sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                myWeb.user = id.toInt()
                Log.e("SPINNER", selectedItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                Log.e("SPINNER", "fff")
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.BT_MAP->{
                Log.e("Button", "MAP")
                var it = Intent(this, MapsActivity::class.java)
                startActivity(it)
            }
            R.id.BT_DATA->{
                Log.e("Button", "DATA")
                var it = Intent(this, ShowDataActivity::class.java)
                startActivity(it)
            }
        }
    }

}


