package com.example.final_project

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import org.w3c.dom.Text
import java.lang.Exception

class ShowDataActivity : AppCompatActivity(), View.OnClickListener {
    var num: Int = 0
    private lateinit var txv: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_data)

        // Orientation will not change when the cell phone tilt.
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        // Cell phone will keep screen on.
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        txv = findViewById(R.id.TXV_SHOW)

        // Get the bike user
        myWeb.updateBikeUsers()
        // Create Adapter
        var arrAda = ArrayAdapter(this, R.layout.my_spinner_style, myWeb.bikeUser)
        var sp:Spinner = findViewById(R.id.SPINNER2)
        arrAda.setDropDownViewResource(R.layout.my_spinner_style)
        sp.adapter = arrAda
        sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                myWeb.user = id.toInt()
                Log.e("SPINNER", selectedItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                Log.e("SPINNER", "fff")
            }
        }

        // Button
        var bt:Button = findViewById(R.id.BT_BACK)
        bt.setOnClickListener(this)
        bt = findViewById(R.id.BT_SEARCH)
        bt.setOnClickListener(this)

        // EditText
        var edt: EditText = findViewById(R.id.EDT_NUM)
        edt.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                Log.e("EditText", "After")
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                Log.e("EditText", edt.text.toString())

                try {
                    num = edt.text.toString().toInt()
                }
                catch (e: Exception){
                    Toast.makeText(applicationContext, "Please enter integer", Toast.LENGTH_SHORT).show()
                }

            }
        })
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.BT_BACK->{
                Log.e("BUTTON", "BACK")
                var it = Intent(this, MainActivity::class.java)
                startActivity(it)
                myWeb.clearArray()
                finish()
            }
            R.id.BT_SEARCH->{
                Log.e("BUTTON", "SEARCH")

                var showStr: String = ""
                myWeb.clearArray()
                val json_payload = myWeb.getJsonArray(myWeb.getURL(myWeb.user, num))
                myWeb.updateLatLon(json_payload)
                if(myWeb.lat.size > 0) {
                    for (i in 0 until myWeb.lat.size) {
                        showStr += "➵${myWeb.timeTag.get(i)} ${myWeb.lat.get(i)},${myWeb.lon.get(i)}\n"
                    }
                    txv.setText(showStr)
                }
                else {
                    txv.setText("No data found !!!")
                }
                if(myWeb.lat.size != num) {
                    Toast.makeText(applicationContext, "Data only has ${myWeb.lat.size}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}
