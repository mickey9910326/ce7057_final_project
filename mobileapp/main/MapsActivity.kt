package com.example.final_project

import android.animation.ValueAnimator
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.createBitmap
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*


const val DEFAULT_ZOOM_SCALE = 16f
const val NCU_ME_LAT = 24.967689
const val NCU_ME_LON = 121.188503

var myWeb = web_api()
val POLYLINE_WIDTH = 10F
val POLYLINE_ANIMATION_POINTS = 20

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener, SeekBar.OnSeekBarChangeListener, OnTouchListener, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var bt:Button
    private lateinit var txv_progress:TextView
    private lateinit var tAnimator: ValueAnimator
    private lateinit var polyOption: PolylineOptions
    private lateinit var marker:Marker
    private var dotN: Int = 0
    private var posN: Int = 1
    private var posMax: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // Orientation will not change when the cell phone tilt.
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        // Cell phone will keep screen on.
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        bt = findViewById(R.id.BT_UPDATE)
        var sb:SeekBar = findViewById(R.id.SEEKBAR)
        bt.setOnClickListener(this)
        bt.setOnTouchListener(this)
        sb.setOnSeekBarChangeListener(this)
        txv_progress = findViewById(R.id.TEXTVIEW_PROGRESS)
        myWeb.seekBarProgrss = sb.progress

        // Toolbar initialize
        var tl:Toolbar = findViewById(R.id.TOOLBAR)
        tl.setNavigationIcon(R.drawable.back_lite)
        tl.setNavigationOnClickListener {
            Log.e("BACK", "CLICK")
            backToMain()
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.uiSettings.isCompassEnabled = true
        googleMap.uiSettings.isMyLocationButtonEnabled = true
        googleMap.uiSettings.isMapToolbarEnabled = true

        val json_payload = myWeb.getJsonArray(myWeb.getURL(myWeb.user, myWeb.seekBarProgrss))
        myWeb.updateLatLon(json_payload)
        // the newest marker
        var lat_zm: Double = 0.0
        var lon_zm: Double = 0.0
        if(myWeb.lat.size > 0) {
            lat_zm = myWeb.lat.get(0)
            lon_zm = myWeb.lon.get(0)
            creteMarker(lat_zm, lon_zm, myWeb.timeTag.get(0)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.bike_lite))
        }
        else {
            lat_zm = NCU_ME_LAT
            lon_zm = NCU_ME_LON
            Toast.makeText(applicationContext, "No data found !!!", Toast.LENGTH_SHORT).show()
        }

        var cameraPos = CameraPosition.builder()
            .target(LatLng(lat_zm, lon_zm))
            .bearing(0F)
            .tilt(0F)
            .zoom(DEFAULT_ZOOM_SCALE)
            .build()

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos), 1000, null)
        mMap.setOnMarkerClickListener(this)

        tAnimator = ValueAnimator.ofInt(0, 1)
        tAnimator.repeatCount = Animation.INFINITE
        tAnimator.repeatMode = ValueAnimator.RESTART
        tAnimator.interpolator = LinearInterpolator()
        tAnimator.addUpdateListener {
            // animate here
            if (posMax > 1) {
                val lat1 = myWeb.lat.get(posN)
                val lon1 = myWeb.lon.get(posN)
                val lat2 = myWeb.lat.get(posN-1)
                val lon2 = myWeb.lon.get(posN-1)
                val delta_lat = (lat2 - lat1) / POLYLINE_ANIMATION_POINTS
                val delta_lon = (lon2 - lon1) / POLYLINE_ANIMATION_POINTS
                var pos1 = LatLng(
                    lat1 + dotN * delta_lat,
                    lon1 + dotN * delta_lon
                )
                var pos2 = LatLng(
                    lat1 + (dotN + 1) * delta_lat,
                    lon1 + (dotN + 1) * delta_lon
                )
                polyOption.add(pos1, pos2)
                mMap.addPolyline(polyOption)
                if(dotN == 0) {
                    // first marker create
                    if(posN == posMax-1) {
                            marker = creteMarker(
                            myWeb.lat.get(posN),
                            myWeb.lon.get(posN),
                            myWeb.timeTag.get(0)  // this marker will run to newest position
                        )
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.bike_lite))
                    }
                    creteMarker(
                        myWeb.lat.get(posN),
                        myWeb.lon.get(posN),
                        myWeb.timeTag.get(posN)
                    )
                }
                marker.position = pos2
                dotN++
                if (dotN == POLYLINE_ANIMATION_POINTS) {
                    dotN = 0
                    posN--
                }
                if (posN == 0) {
                    posN = posMax - 1
                    tAnimator.pause()

                }
            }
            // Choose only one bike or empty
            else {
                if(myWeb.lat.size > 0) {
                    lat_zm = myWeb.lat.get(0)
                    lon_zm = myWeb.lon.get(0)
                    creteMarker(lat_zm, lon_zm, myWeb.timeTag.get(0)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.bike_lite))
                }
                tAnimator.pause()
            }
        }

        Log.e("MAP", "Ready")
    }

    fun initAnimationParameters(){
        dotN = 0
        posN = 1
        posMax = 0
    }

    fun creteMarker(lat: Double, lon: Double, posTitle: String): Marker{
        var pos = LatLng(lat, lon)
        val marker = mMap.addMarker(MarkerOptions().position(pos).title(posTitle))
        return marker
    }



    fun backToMain() {
        var it = Intent(this, MainActivity::class.java)
        startActivity(it)
        tAnimator.pause()
        tAnimator.end()
        mMap.clear()         // clear marker
        myWeb.clearArray()   // clear array list
        finish()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        Log.e("BACK", "CLICK")
        backToMain()
        return super.onKeyDown(keyCode, event)
    }

    /**
     * Button click listener
     */
    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.BT_UPDATE-> {
                tAnimator.pause()
                initAnimationParameters()
                Log.e("BUTTON", "UPDATE")
                mMap.clear()         // clear marker
                myWeb.clearArray()   // clear array list
                val json_payload = myWeb.getJsonArray(myWeb.getURL(myWeb.user, myWeb.seekBarProgrss))
                myWeb.updateLatLon(json_payload)
                polyOption = PolylineOptions()
                polyOption.width(POLYLINE_WIDTH)
                polyOption.color(Color.BLACK)
                polyOption.startCap(SquareCap())
                polyOption.endCap(SquareCap())
                posMax = myWeb.lat.size
                posN = posMax -1

                if(myWeb.lat.size != myWeb.seekBarProgrss) {
                    Toast.makeText(applicationContext, "Data only has ${myWeb.lat.size}", Toast.LENGTH_SHORT).show()
                }
                tAnimator.start()
            }
        }
    }

    /**
     * Seekbar change listener
     */
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        Log.e("Seekbar", progress.toString())
        myWeb.seekBarProgrss = progress
        txv_progress.setText(progress.toString())
    }
    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }

    /**
     * Button press and release events
     */
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when(v?.id) {
            R.id.BT_UPDATE-> {
                when(event?.action) {
                    MotionEvent.ACTION_DOWN-> {
                        Log.e("UPDATE", "DOWN")
                        bt.setText("Waiting")
                        bt.setTextColor(resources.getColor(R.color.Black))
                        bt.setBackgroundColor(resources.getColor(R.color.White))
                    }
                    MotionEvent.ACTION_UP-> {
                        bt.setText("Update")
                        bt.setTextColor(resources.getColor(R.color.White))
                        bt.setBackgroundColor(resources.getColor(R.color.Black))
                    }
                }
            }
        }
        return false
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        Log.e("MARKER", marker.toString())
        marker!!.showInfoWindow()
        return true
    }


}
