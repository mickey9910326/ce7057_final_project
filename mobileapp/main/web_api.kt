package com.example.final_project

import android.content.res.Resources
import android.service.voice.AlwaysOnHotwordDetector
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import androidx.core.content.ContextCompat
import com.squareup.okhttp.Callback
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.Response
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException

class web_api : AppCompatActivity() {
    val url = "http://140.115.66.249:4848/api/gr/bid/"
    val TIIMEOUT = 3000L

    var user = 0
    var lat = ArrayList<Double>()
    var lon = ArrayList<Double>()
    var timeTag = ArrayList<String>()
    var seekBarProgrss = 5                  // default, show 5 markers
    var bikeUser = ArrayList<String>()

    fun getJsonArray(url: String): JSONArray{
        var json_payload = JSONArray()
        var flag = false
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(url)
            .build()
        Log.e("URL", url)
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(request: Request?, exception: IOException?) {
                Log.e("URL", "IOException")}

            override fun onResponse(response: Response) {
                try {
                    val responseData = response.body().string()
                    json_payload = JSONArray(responseData)
                    flag = true
                } catch (e: JSONException) {
                    Log.e("URL", "JSONEXception")}
            }
        })
        val now = System.currentTimeMillis()
        while((!flag) and ((System.currentTimeMillis() - now) < TIIMEOUT)) {

        }
        Log.e("json_payload", json_payload.toString())
        return json_payload
    }

    fun updateLatLon(payload: JSONArray) {
        for(i in 0 until payload.length()) {
            lat.add(i, payload.getJSONObject(i).getString(KEY_LATITUDE).toDouble())
            lon.add(i, payload.getJSONObject(i).getString(KEY_LONGITUDE).toDouble())
            timeTag.add(i, payload.getJSONObject(i).getString(KEY_TIMETAG))
        }
    }

    fun clearArray() {
        lat.clear()
        lon.clear()
        timeTag.clear()
    }

    fun getURL(user_id: Int, show_cnt: Int): String {
        return url + (user_id+1).toString() + "/count/" + show_cnt.toString()
    }

    fun updateBikeUsers() {
        bikeUser.clear()
        var payload = getJsonArray("http://140.115.66.249:4848/api/bike/")
        for (n in 0 until payload.length()) {
            bikeUser.add(payload.getJSONObject(n).getString(KEY_NAME))
        }
        if(bikeUser.size < 1) {
            Log.e("BIKEUSER", "Empty")
        }
        Log.e("BIKEUSER", bikeUser.toString())
    }

}