from environs import Env

__all__ = ['ENV']

ENV = Env()
ENV.read_env()
