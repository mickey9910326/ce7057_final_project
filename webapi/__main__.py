from .settings import ENV
from .db import db
from .app import app
from .api.v1.gps_record import blue_rest_gr
from .api.v1.bike import blue_rest_bike


def init():
    app.config['SERVER_NAME'] = ENV('WEBAPI_HOST') + ':' + ENV('WEBAPI_PORT')
    app.config['DEBUG'] = ENV.bool("WEBAPI_DEBUG", False)
    app.config['SQLALCHEMY_DATABASE_URI'] = ENV('DATABASE_URI')
    db.init_app(app)
    app.register_blueprint(blue_rest_gr, url_prefix='/api/gr')
    app.register_blueprint(blue_rest_bike, url_prefix='/api/bike')


def run():
    init()
    app.run()


if __name__ == "__main__":
    run()
