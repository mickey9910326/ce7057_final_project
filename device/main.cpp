#include <time.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "DS3231.h"
#include "GroveGPS.h"
#include "RadioEvent.h"
#include "dot_util.h"
void read_gps();
using namespace std;
static uint8_t config_app_eui[] = {0x86, 0xe4, 0xef, 0xc7,
                                   0x10, 0x4f, 0x68, 0x29};
static uint8_t config_app_key[] = {0xa3, 0x46, 0xb6, 0xfa, 0xef, 0x2b,
                                   0xd3, 0x3c, 0x16, 0xfe, 0x9b, 0x1d,
                                   0x8d, 0x47, 0xa1, 0x1d};
// USER ADD
char clock_str[80] = {0};
GroveGPS gps(PA_2, PA_3);
char latBuffer[16], lonBuffer[16];
rtl hrtl = {0};
// use the same subband as gateway that gateway can listen to you
static uint8_t config_frequency_sub_band = 1;
mDot *dot;
lora::ChannelPlan *plan = NULL;
void Json_string(stringstream *s, char *lon, char *lat, char *clock);
#if ACTIVE_EXAMPLE == OTA_EXAMPLE
void config(mDot *dot, lora::ChannelPlan *plan) {
    uint8_t ret;
    //  reset to default config so we know what state we're in
    dot->resetConfig();
    //  set how many log info will be show
    dot->setLogLevel(mts::MTSLog::INFO_LEVEL);
    // set subband frequency the same as gateway so gateway can listen to you
    logInfo("setting frequency sub band\r\n");
    if ((ret = dot->setFrequencySubBand(config_frequency_sub_band)) !=
        mDot::MDOT_OK) {
        logError("failed to set frequency sub band %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
    }
    // lora has private network and public network here we use public network
    logInfo("setting public network mode");
    if ((ret = dot->setPublicNetwork(true)) != mDot::MDOT_OK) {
        logError("failed to public network mode");
    }
    std::vector<uint8_t> temp;

    for (int i = 0; i < 8; i++) {
        temp.push_back(config_app_eui[i]);
    }
    // set network id
    logInfo("setting app eui\r\n");
    if ((ret = dot->setNetworkId(temp)) != mDot::MDOT_OK) {
        logError("failed to set app eui %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
    }
    temp.clear();
    for (int i = 0; i < 16; i++) {
        temp.push_back(config_app_key[i]);
    }
    // set network key
    logInfo("setting app key\r\n");
    if ((ret = dot->setNetworkKey(temp)) != mDot::MDOT_OK) {
        logError("failed to set app key %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
    }
    // a higher spreading factor allows for longer range but lower throughput
    // in the 915 (US) frequency band, spreading factors 7 - 10 are available
    logInfo("setting TX spreading factor\r\n");
    if ((ret = dot->setTxDataRate(mDot::DR4)) != mDot::MDOT_OK) {
        logError("failed to set TX datarate %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
    }
    // request receive confirmation of packets from the gateway
    logInfo("enabling ACKs\r\n");
    if ((ret = dot->setAck(1)) != mDot::MDOT_OK) {
        logError("failed to enable ACKs %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
    }
    // Set Tx Power
    logInfo("enabling Tx Power\r\n");
    if ((ret = dot->setTxPower(20)) != mDot::MDOT_OK) {
        logError("failed to enable Tx Power %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
    }
    // request receive confirmation of packets from the gateway
    logInfo("enabling Tx Data Rate\r\n");
    if ((ret = dot->setTxDataRate(3)) != mDot::MDOT_OK) {
        logError("failed to enable Tx Data Rate %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
    }
    // save this configuration to the mDot's NVM
    logInfo("saving config\r\n");
    if (!dot->saveConfig()) {
        logError("failed to save configuration\r\n");
    }
    logInfo("joining network\r\n");
    while ((ret = dot->joinNetwork()) != mDot::MDOT_OK) {
        logError("failed to join network %d:%s\r\n", ret,
                 mDot::getReturnCodeString(ret).c_str());
        osDelay(std::max((uint32_t)1000, (uint32_t)dot->getNextTxMs()));
    }
    return;
}
int main() {
    plan = new lora::ChannelPlan_AS923();

    dot = mDot::getInstance(plan);

    // set network
    config(dot, plan);

    while (true) {
        int ret;
        std::vector<uint8_t> data;
        stringstream ss;
        std::string data_str;
        DS3231_get(&hrtl);
        struct tm clock_t;
        clock_t.tm_sec = hrtl.sec;
        clock_t.tm_min = hrtl.min;
        clock_t.tm_hour = hrtl.hour;
        clock_t.tm_mon = hrtl.mon - 1;
        clock_t.tm_mday = hrtl.day;
        clock_t.tm_year = (hrtl.year) + 100;
        strftime(clock_str, 80, "%m/%d/%Y,%H:%M:%S", &clock_t);
        read_gps();
        Json_string(&ss, lonBuffer, latBuffer, clock_str);
        ss >> data_str;
        for (std::string::iterator it = data_str.begin(); it != data_str.end();
             it++)
            data.push_back((uint8_t)*it);

        // send the data to the gateway
        if ((ret = dot->send(data)) != mDot::MDOT_OK) {
            logError("failed to send\r\n", ret,
                     mDot::getReturnCodeString(ret).c_str());
        } else {
            logInfo("successfully sent data to gateway\r\n");
        }
        osDelay((uint32_t)30000);
    }

    return 0;
}