from .settings import ENV
from .create_gr import create_gr

import paho.mqtt.client as mqtt
import json
import threading
import base64

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("application/1/device/008000000000c077/rx")


def on_message(client, userdata, msg):
    # print(msg.topic+" "+str(msg.payload))
    m = json.loads(msg.payload)
    print(m)
    d = json.loads(base64.b64decode(m['data']))
    print(d)

    t = threading.Thread(target=job_create_gr, args=(d,))
    t.start()

def job_create_gr(data):
    create_gr(1, data['lat'], data['lon'], data['timetag'])


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set(ENV('MQTT_USER'), ENV('MQTT_PASS'))
if ENV('MQTT_EN_TLS'):
    client.tls_set()
client.connect(ENV('MQTT_HOST'), int(ENV('MQTT_PORT')), 60)

client.loop_forever()
