__all__ = ['ENV']

from environs import Env

ENV = Env()
ENV.read_env()
