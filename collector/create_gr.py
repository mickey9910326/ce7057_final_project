import requests
import datetime
from .settings import ENV


def create_gr(bike_id, lat, lon, timetag):
    url = "http://{}:{}/api/gr/".format(ENV('WEB_HOST'), ENV('WEB_PORT'))
    data = {
        'bike_id': bike_id,
        'lat': lat,
        'lon': lon,
        'timetag': timetag
    }
    headers = {
        'Access-Token': ENV('SYS_PASS')
    }
    r = requests.post(url, data, headers=headers)
    print(r)


if __name__ == "__main__":
    create_gr(3, 10, 10, datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
