from flask import Blueprint, request, jsonify
from ...models import Bike
from ...db import db
from sqlalchemy import desc

blue_rest_bike = Blueprint('blue_rest_bike', __name__)


@blue_rest_bike.route('/', methods=['GET'])
def bike_get_all():
    bikes = Bike.query.all()
    res = [bike.json() for bike in bikes]
    return jsonify(res)


@blue_rest_bike.route('/<bid>', methods=['GET'])
def get_bike_by_id(bid):
    bike = Bike.query.filter_by(id=bid).first()
    res = bike.json()
    return jsonify(res)


@blue_rest_bike.route('/<bid>/name', methods=['GET'])
def get_bike_name_by_id(bid):
    bike = Bike.query.filter_by(id=bid).first()
    res = bike.json()['name']
    return res
