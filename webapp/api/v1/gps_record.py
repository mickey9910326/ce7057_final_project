from flask import Blueprint, request, jsonify
from ...models import GpsRecord
from ...db import db
from ...settings import ENV
from sqlalchemy import desc

blue_rest_gr = Blueprint('blue_rest_gr', __name__)


@blue_rest_gr.route('/', methods=['POST'], strict_slashes=False)
def gr_create():
    if not 'Access-Token' in request.headers.keys():
        return '402'
    if request.headers['Access-Token'] != ENV('SYS_PASS'):
        return '401'
    if len(request.values) == 0:
        return '200'
    else:
        gr = GpsRecord(**request.values)
        db.session.add(gr)
        db.session.commit()
        return jsonify(gr.json())


@blue_rest_gr.route('/', methods=['GET'])
def gr_get_all():
    grs = GpsRecord.query.all()
    res = [gr.json() for gr in grs]
    return jsonify(res)


@blue_rest_gr.route('/bid/<bid>', methods=['GET'])
def get_gr_by_bid(bid):
    grs = GpsRecord.query.filter_by(
        bike_id=bid).order_by(desc(GpsRecord.timetag))
    res = [gr.json() for gr in grs]
    return jsonify(res)


@blue_rest_gr.route('bid/<bid>/count/<num>', methods=['GET'])
def get_gr_by_bid_and_num(bid, num):
    grs = GpsRecord.query.filter_by(
        bike_id=bid).order_by(desc(GpsRecord.timetag)).limit(num)
    res = [gr.json() for gr in grs]
    return jsonify(res)
