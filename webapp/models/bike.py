from ..db import db
import datetime

__all__ = ['Bike']


class Bike(db.Model):
    __tablename__ = 'bike'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.VARCHAR(255))
    created_at = db.Column(db.Date, default=datetime.datetime.utcnow)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Bike '{}', id: {}>".format(self.name, self.id)

    def json(self):
        res = {
            'id': self.id,
            'name': self.name
        }
        return res
