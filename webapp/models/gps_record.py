from ..db import db
import datetime

__all__ = ['GpsRecord']


class GpsRecord(db.Model):
    __tablename__ = 'gps_record'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    bike_id = db.Column(db.Integer)
    lat = db.Column(db.Integer)
    lon = db.Column(db.Integer)
    timetag = db.Column(db.Date)
    created_at = db.Column(db.Date, default=datetime.datetime.utcnow)

    def __init__(self, bike_id, lat, lon, timetag):
        self.bike_id = bike_id
        self.lat = lat
        self.lon = lon
        self.timetag = timetag

    def __repr__(self):
        return '<GpsRecord {}, bike_id {}, lat: {}, lan: {}, t: {}>'.format(
            self.id, self.bike_id, self.lat, self.lon, self.timetag)

    def json(self):
        res = {
            'id': self.id,
            'bike_id': self.bike_id,
            'lat': self.lat,
            'lon': self.lon,
            'timetag': self.timetag
        }
        return res
