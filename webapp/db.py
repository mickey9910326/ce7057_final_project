from .settings import ENV
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
