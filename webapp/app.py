from flask import Flask, request, render_template, jsonify
from .settings import ENV
from .db import db
from .models import Bike, GpsRecord

app = Flask(__name__)

@app.route('/', methods=['GET'])
def root():
    return render_template('main.html')
